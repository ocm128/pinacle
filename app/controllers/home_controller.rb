class HomeController < ApplicationController

  layout "indice"

  before_action :correct_user,   only: [:edit, :update]
  #before_action :authenticate_admin!, only: [:destroy]

  def index
    @users = User.all
  end

  def about
  end

  def destroy
  end

end
