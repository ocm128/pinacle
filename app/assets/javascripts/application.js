// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
// require_self
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require jquery.easing
//= require bootstrap
//= require bootstrap-waterfall
//= require grayscale
// require pins
// require masonry/jquery.masonry
//= require turbolinks
// require_tree .
// require grayscale-sass/assets/bower_components/jquery.easing/js/jquery.easing.js
// require grayscale-sass/assets/bower_components/bootstrap/dist/js/bootstrap.js
// require grayscale-sass/assets/js/grayscale.js

$(document).ready(function() {
  $('.has-tooltip').tooltip();
});

