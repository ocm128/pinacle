class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :pins, dependent: :destroy # Si un usuario borra su cuenta se borran sus pins

  validates :name, presence: true, length: {in: 4..20}, uniqueness: { case_sensitive: false }

  #validates :name, presence: true, length: {in: 4..20}, uniqueness: { case_sensitive: false },
    #format: { with: /\A[a-zA-Z0-9]*\z/, message: "Solo puede contener letras y números" }
end
